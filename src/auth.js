import { ref } from "vue";

// make two references
const loading = ref(true);
const isAuthenticated = ref(false);
const currentUser = ref(null);

const options = {
  credentials: "include",
};
// check authentication
fetch(`${import.meta.env.VITE_BACKEND_URL}/ping`, options)
  .then((res) => {
    // once loaded, set loading to false
    loading.value = false;
    // if auth ok, set isAuthenticated to true
    if (res.ok) {
      isAuthenticated.value = true;
      return res.json();
    }
  })
  .then((json) => {
    if (json) {
      currentUser.value = json.user.username;
    }
  });

export { loading, isAuthenticated, currentUser };
