import * as Vue from "vue";
import * as VueRouter from "vue-router";
import App from "./App.vue";
import { routes } from "./router/index.js";
import "./styles/style.scss";

// set up router
const router = VueRouter.createRouter({
  history: VueRouter.createWebHashHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
}),
  Vue.createApp(App).use(router).mount("#app");
