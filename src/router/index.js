import Home from "../views/Home.vue";
import Post from "../views/Post.vue";
import EditPost from "../views/EditPost.vue";
import Login from "../views/Login.vue";
import Signup from "../views/Signup.vue";
import User from "../views/User.vue";
import { useRouter } from "vue-router";

const router = useRouter();

export const routes = [
  { path: "/", component: Home, meta: { title: "myPlace()" } },
  {
    path: "/post/:postID",
    component: Post,
    meta: { title: "myPlace() - Post" },
  },
  {
    path: "/post/:postID/edit",
    component: EditPost,
    meta: { title: "myPlace() - Editing Post" },
  },
  { path: "/login", component: Login, meta: { title: "myPlace() - Login" } },
  {
    path: "/user/new_user",
    component: Signup,
    meta: { title: "myPlace() - Join" },
  },
  {
    path: "/user/:userID",
    component: User,
    meta: { title: "myPlace() - User" },
  },
];
