# vue-blog-frontend

Frontend for express-blog-api made with Vue and Tailwind CSS.

Hosted on [Netlify](https://singular-malabi-cf37ee.netlify.app/#/)
